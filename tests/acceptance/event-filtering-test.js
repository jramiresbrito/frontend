import { module, test } from 'qunit';
import { click, visit, fillIn, findAll, currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { Interactor as Pikaday } from 'ember-pikaday/test-support';

module('Acceptance | event filtering', function (hooks) {
  setupApplicationTest(hooks);

  test('visiting /', async function (assert) {
    await visit('/');

    assert.equal(currentURL(), '/');
    assert.dom('[data-test-home-button]').containsText('Home');
    assert.dom('[data-test-new-event-button]').containsText('Novo Evento');
    assert.dom('[data-test-name-input]').exists();
    assert.dom('[data-test-type-input]').exists();
    assert.dom('[data-test-calendar]').exists();
    let allEvents = findAll('[data-test-event]');
    assert.equal(allEvents.length, 10);
  });

  test('it should be able to filter events by name', async function (assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    let eventName = 'Evento #10';
    await fillIn('[data-test-name-input] input', eventName);
    let allEvents = findAll('[data-test-event]');
    assert.equal(allEvents.length, 1);

    let searchParams = encodeURIComponent(eventName);
    assert.equal(currentURL(), `/?name=${searchParams}`);

    assert.dom('[data-test-event] span').containsText(eventName);
  });

  test('it should be able to filter events by type', async function (assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    let eventType = 'teatro';
    await fillIn('[data-test-type-input] input', eventType);
    let eventsImages = findAll('[data-test-event] img');
    let imgSrc = 'http://localhost:7357/assets/images/teatro.png';
    eventsImages.forEach((event) => assert.equal(event.src, imgSrc));

    let searchParams = encodeURIComponent(eventType);
    assert.equal(currentURL(), `/?type=${searchParams}`);
  });

  test('it should be able to filter events by date', async function (assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    await click('[data-test-calendar] input');
    await Pikaday.selectDate(new Date(2021, 2, 11));
    let searchParams = encodeURIComponent('11-03-2021');

    assert.equal(currentURL(), `/?starts_at=${searchParams}`);
    let filteredEventsDate = findAll('[data-test-event-starts-at]');
    let expectedDateString = 'Início: 10-03-2021';

    filteredEventsDate.forEach((event) =>
      assert.equal(event.innerText, expectedDateString)
    );
  });

  test('it should be able to filter by combined attributes', async function (assert) {
    await visit('/');

    assert.equal(currentURL(), '/');

    let nameFilter = 'evento';
    let typeFilter = 'teatro';
    await fillIn('[data-test-name-input] input', nameFilter);
    await fillIn('[data-test-type-input] input', typeFilter);

    let nameParam = encodeURIComponent(nameFilter);
    let typeParam = encodeURIComponent(typeFilter);
    assert.equal(currentURL(), `/?name=${nameParam}&type=${typeParam}`);

    let eventsNames = findAll('[data-test-event-name]');
    eventsNames.forEach((event) =>
      assert.equal(event.innerText.split(' ')[0], 'Evento')
    );

    let eventsImages = findAll('[data-test-event] img');
    let imgSrc = `http://localhost:7357/assets/images/${typeFilter}.png`;
    eventsImages.forEach((event) => assert.equal(event.src, imgSrc));
  });
});
