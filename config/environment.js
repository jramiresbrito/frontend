'use strict';

module.exports = function (environment) {
  let ENV = {
    modulePrefix: 'frontend',
    environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false,
      },
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
  }

  ENV['ember-simple-auth-token'] = {
    tokenPropertyName: 'token', // Key in server response that contains the access token
    refreshTokenPropertyName: 'token', // Key in server response that contains the refresh token
    tokenDataPropertyName: 'tokenData', // Key in session to store token data
    refreshAccessTokens: true, // Enables access token refreshing
    tokenExpirationInvalidateSession: true, // Enables session invalidation on token expiration
    serverTokenEndpoint: 'https://localhost:3000/api/v1/sign_in',
    serverTokenRefreshEndpoint: 'https://localhost:3000/api/v1/sign_in', // Server endpoint to send refresh request
    tokenExpireName: 'exp', // Field containing token expiration
    refreshLeeway: 600, // Amount of time to send refresh request before token expiration
    authorizationHeaderName: 'Authorization', // Header name added to each API request
    authorizationPrefix: 'Bearer ', // Prefix added to each API request
  };

  ENV['ember-paper'] = {
    'paper-toaster': {
      position: 'top right',
      duration: 5000,
      toastClass: 'my-app-toast',
    },
  };

  ENV['HOST'] = 'http://localhost:4200';

  return ENV;
};
