import Helper from '@ember/component/helper';

export const dateFormatter = (date) => {
  let year = date.getFullYear(),
    month = ('0' + (date.getMonth() + 1)).slice(-2),
    day = ('0' + date.getDate()).slice(-2),
    formatted = `${day}-${month}-${year}`;

  return formatted;
};

export default class DateFormatter extends Helper {
  compute(params) {
    let [date] = params;
    date = new Date(date);

    return dateFormatter(date);
  }
}
