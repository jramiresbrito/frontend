import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class IndexRoute extends Route {
  @service store;

  queryParams = {
    page: {
      refreshModel: true,
      replace: true,
    },
    size: {
      refreshModel: true,
      replace: true,
    },
    name: {
      refreshModel: true,
      replace: true,
    },
    type: {
      refreshModel: true,
      replace: true,
    },
    starts_at: {
      refreshModel: true,
      replace: true,
    },
  };

  async model(params) {
    let events = await this.store.query('event', {
      page: {
        number: params.page,
        size: params.size,
      },
      search: {
        name: params.name,
        type: params.type,
        starts_at: params.starts_at,
      },
      include: 'comments',
    });

    return events;
  }
}
