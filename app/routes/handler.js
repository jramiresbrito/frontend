import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class HandlerRoute extends Route {
  @service session;

  queryParams = {
    action: {
      refreshModel: true,
    },
    resource_type: {
      refreshModel: true,
    },
    resource_id: {
      refreshModel: true,
    },
    token: {
      refreshModel: true,
    },
  };

  beforeModel() {
    let eventParams = this.paramsFor('handler');

    if (eventParams.resource_type === 'users') {
      if (eventParams.action === 'create') this.transitionTo('login');
    }

    if (eventParams.resource_type === 'comments') {
      if (eventParams.action === 'create')
        this.transitionTo('event', eventParams.resource_id);
    }
  }
}
