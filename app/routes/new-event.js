import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class NewEventRoute extends Route {
  @service session;

  beforeModel() {
    if (this.session.isAuthenticated == false) {
      this.transitionTo('login');
    }
  }
}
