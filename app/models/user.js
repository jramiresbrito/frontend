import Model, { attr, hasMany } from '@ember-data/model';

export default class UserModel extends Model {
  @attr name;
  @attr email;
  @attr password;
  @attr('string', { defaultValue: 'http://localhost:4200/login' }) sign_in_url;

  @hasMany('event') events;
}
