import Model, { attr, hasMany, belongsTo } from '@ember-data/model';

export default class EventModel extends Model {
  @attr name;
  @attr description;
  @attr type;
  @attr url;
  @attr('boolean') continuous;
  @attr('date') starts_at;
  @attr('date') finishes_at;

  @hasMany('comment') comments;
  @belongsTo('user') user;

  get sortedComments() {
    return this.comments.sortBy('created_at').reverse();
  }
}
