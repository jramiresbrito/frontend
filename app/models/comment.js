import Model, { attr, belongsTo } from '@ember-data/model';

export default class CommentModel extends Model {
  @attr name;
  @attr content;
  @attr email;

  @belongsTo('event') event;
}
