import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class EventFormComponent extends Component {
  @service store;
  @service router;

  @action createEvent() {
    let {
      name,
      description,
      type,
      url,
      starts_at,
      finishes_at,
      continuous,
    } = this;

    continuous ??= false;
    starts_at = new Date(starts_at);
    finishes_at = finishes_at ? new Date(finishes_at) : null;

    let newRecord = this.store.createRecord('event', {
      name,
      description,
      type,
      url,
      continuous,
      starts_at,
      finishes_at,
    });

    newRecord.save().then(
      () => {
        this.router.transitionTo('event', newRecord.id);
      },
      () => {
        // error , display a toast?
      }
    );
  }

  get eventTypes() {
    return ['concerto', 'teatro', 'museu'];
  }
}
