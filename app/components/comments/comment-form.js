import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class CommentFormComponent extends Component {
  @service store;
  @service router;

  @tracked event = null;
  @tracked name;
  @tracked email;
  @tracked content;

  @action createComment(event) {
    let { name, email, content } = this;
    let newRecord = this.store.createRecord('comment', {
      name,
      email,
      content,
      event,
    });
    newRecord.save();

    this.name = null;
    this.email = null;
    this.content = null;
    document.querySelectorAll('input')[0].focus();
  }
}
