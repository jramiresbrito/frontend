import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default class IndexController extends Controller {
  queryParams = ['page', 'size', 'name', 'type', 'starts_at'];
  page = 1;
  size = 10;
  name = '';
  type = '';
  starts_at = '';

  @computed(
    'model.meta.pagination.last.number',
    'model.meta.pagination.self.number'
  )
  get count() {
    const total =
      this.model.meta.pagination.last.number ||
      this.model.meta.pagination.self.number;
    if (!total) return [];
    return new Array(total + 1)
      .join('x')
      .split('')
      .map((e, i) => i + 1);
  }
}
