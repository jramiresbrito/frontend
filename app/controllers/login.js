import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class LoginController extends Controller {
  @service session;

  email = null;
  password = null;
  duration = 3500;
  position = 'top right';
  toastText = 'Credenciais Inválidas!';
  @tracked isToastOpen = false;

  @action authenticate() {
    const credentials = { email: this.email, password: this.password };
    const authenticator = 'authenticator:jwt';

    this.session.authenticate(authenticator, credentials).then(
      () => {
        this.transitionToRoute('index');
      },
      () => (this.isToastOpen = true)
    );
  }
}
