import EmberRouter from '@ember/routing/router';
import config from 'frontend/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('not-found', { path: '/*path' });
  this.route('event', { path: '/events/:event_id' });
  this.route('new-event', { path: '/events/new' });
  this.route('login');
  this.route('handler');
});
